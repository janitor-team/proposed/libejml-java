Source: libejml-java
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders:
 Andreas Tille <tille@debian.org>,
Section: java
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 default-jdk,
 javahelper,
 junit5,
 libauto64fto32f-java,
 libjsr305-java,
 maven-repo-helper,
Standards-Version: 4.3.0
Vcs-Browser: https://salsa.debian.org/java-team/libejml-java
Vcs-Git: https://salsa.debian.org/java-team/libejml-java.git
Homepage: http://ejml.org/wiki

Package: libejml-java
Architecture: all
Depends:
 ${misc:Depends},
Description: Efficient Java Matrix Library
 Efficient Java Matrix Library (EJML) is a linear algebra library for
 manipulating dense matrices. Its design goals are;
  1) to be as computationally and memory efficient as possible for both
     small and large matrices, and
  2) to be accessible to both novices and experts.
 These goals are accomplished by dynamically selecting the best
 algorithms to use at runtime, clean API, and multiple interfaces.
 .
 EJML has three distinct ways to interact with it:
  1) procedural,
  2) SimpleMatrix, and
  3) Equations.
 Procedure provides all capabilities of EJML and almost complete control
 over memory creation, speed, and specific algorithms. SimpleMatrix
 provides a simplified subset of the core capabilities in an easy to use
 flow styled object-oriented API, inspired by Jama. Equations is a
 symbolic interface, similar in spirit to Matlab and other CAS, that
 provides a compact way of writing equations.
